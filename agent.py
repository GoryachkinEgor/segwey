import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import segwey

class control_agent(nn.Module):
	def __init__(self, model='v1'):
		super().__init__()
		self.env = segwey.segwey(model)
		self.env.set_param(discrete_level = 10, max_u = 20)
		self.network = nn.Sequential()
		self.network.add_module('layer1', nn.Linear(self.env.state_dim[0] + 1, 64))
		self.network.add_module('relu1', nn.ReLU())
		self.network.add_module('layer2', nn.Linear(64, 64))
		self.network.add_module('relu2', nn.ReLU())
		self.network.add_module('layer3', nn.Linear(64, self.env.n_actions))

		self.opt = torch.optim.Adam(self.network.parameters(), lr=1e-4)
		self.epsilon = 0.1

	def reward(self, s, r, u=0):
		return r

	def get_action(self, state, u, epsilon=0):
	    state = torch.tensor((np.hstack([state, u]))[None], dtype=torch.float32)
	    q_values = self.network(state).detach().numpy()
	    action = np.argmax(q_values)
	    if np.random.binomial(1,p=epsilon):
	        action = np.random.choice(range(q_values.shape[-1]))

	    return int(action)

	def compute_td_loss(self, states, actions, control_f, t, rewards, next_states, is_done, gamma=0.99, check_shapes=False):
	    states = torch.tensor(
	        states, dtype=torch.float32)    # shape: [batch_size, state_size]
	    actions = torch.tensor(actions, dtype=torch.long)    # shape: [batch_size]
	    rewards = torch.tensor(rewards, dtype=torch.float32)  # shape: [batch_size]
	    next_states = torch.tensor(next_states, dtype=torch.float32)
	    is_done = torch.tensor(is_done, dtype=torch.uint8)  # shape: [batch_size]

	    state_network_in = torch.tensor(np.hstack([np.array(states),[[control_f(np.array(t).transpose())]]]), dtype=torch.float32)
	    next_states_network_in = torch.tensor(np.hstack([np.array(next_states),[[control_f((np.array(t)+1).transpose())]]]), dtype=torch.float32)
	    predicted_qvalues = self.network(state_network_in)
	    predicted_qvalues_for_actions = predicted_qvalues[range(states.shape[0]), actions]
	    predicted_next_qvalues = self.network(next_states_network_in)
	    next_state_values =  torch.max(predicted_next_qvalues, dim=-1)[0]
	    assert next_state_values.dtype == torch.float32
	    target_qvalues_for_actions =  rewards + gamma*next_state_values
	    target_qvalues_for_actions = torch.where(
	        is_done, rewards, target_qvalues_for_actions)
	    loss = torch.mean((predicted_qvalues_for_actions -
	                       target_qvalues_for_actions.detach()) ** 2)
	    if check_shapes:
	        assert predicted_next_qvalues.data.dim(
	        ) == 2, "make sure you predicted q-values for all actions in next state"
	        assert next_state_values.data.dim(
	        ) == 1, "make sure you computed V(s') as maximum over just the actions axis and not all axes"
	        assert target_qvalues_for_actions.data.dim(
	        ) == 1, "there's something wrong with target q-values, they must be a vector"

	    return loss

	def generate_session(self, u, t_max=1000, epsilon=0, train=True):
	    total_reward = 0
	    s = self.env.reset()
	    for t in range(t_max):
	        a = self.get_action(s, u(t), epsilon=epsilon)
	        next_s, r, done, _ = self.env.step(a)
	        r = self.reward(next_s, r, u(t))
	        if train:
	            self.opt.zero_grad()
	            loss = self.compute_td_loss([s], [a], u, [t], [r], [next_s], [done])
	            loss.backward()
	            self.opt.step()
	        total_reward += r
	        s = next_s
	        if done:
	            break
	    return np.array([total_reward, float(loss)])

	def save(self, name):
		torch.save(self.network.state_dict(), name)

	def load(self, name):
		self.network.load_state_dict(torch.load(name))

